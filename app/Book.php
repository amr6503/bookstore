<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
    	'id',
    	'Publisher',
    	'Name',
    	'Price',
    	'Author',
    	'Description',
    	'Image',
    	'Category'

    ];


    //     public function cart()
    // {
    //     return $this->belongsTo('App\Cart');
    // }


}
