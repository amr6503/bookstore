<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;
use App\Author;
use App\Order;
use App\Cart;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "string";
        return view('admin/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function booksview()
    {
        $Books = DB::table('books')->paginate(10);
        return view('admin/books',['Books'=>$Books]);
    }
    public function booksupdate($id)
    {
        $Books = DB::table('books')->where('id','=',$id)->get();
        return view('admin/booksUpdate',['Books'=>$Books]);
    }
    public function booksCreate()
    {
        return view('admin/bookAdd');
    }
    public function categoriesview()
    {
        $Categories = Category::paginate(10);
        return view('admin.category',['Categories'=>$Categories]);
    }

    public function categoriesupdate($id)
    {
        $Categories = Category::where('Id','=',$id)->get();
        return view('admin/categoryUpdate',['Categories'=>$Categories]);
       
    }
    public function authorsview()
    {
        $Authors = Author::paginate(10);
        return view('admin.author',['Authors'=>$Authors]);
    }

    public function authorsupdate($id)
    {
        $Authors = Author::where('Id','=',$id)->get();
        return view('admin/authorUpdate',['Authors'=>$Authors]);
       
    }

        public function ordersview()
    {
        $Orders = Order::join('books', 'books.id', '=', 'orders.book_id')->get();

        return view('admin.Order',[
            
            'Orders'=>$Orders 
        ]);
    }


    
}
