<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Bestseller;
use App\Newarrival;
use Illuminate\Support\Facades\DB;  

class MixedController extends Controller
{
//   home variables and view
     public function index()
    {
            
            $bestseller =DB::table('bestsellers')
            ->join('books', 'books.id', '=', 'bestsellers.BookId')
             ->get();
             
             $newarrival =DB::table('newarrivals')
            ->join('books', 'books.id', '=', 'newarrivals.BookId')
             ->get();
             
            
        return view('index' , [
            'Bestsellers' => $bestseller ,
            'Newarrivals' => $newarrival

        ]);
        
    }
    public function bookrequest(){
        return view('BookRequestForm');
        
    }
    
   
}
