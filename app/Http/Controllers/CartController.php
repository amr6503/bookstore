<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart ;
use App\Book;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $UserId = auth::id();
        if ($UserId) {
           
                        $Carts = Cart::where('user_id' , '=' , $UserId)
                         ->join('books', 'books.id', '=', 'carts.book_id')
                         ->get();
                        return view('cart',[ 'Carts'=>$Carts ] ); }
        else {
        return view('auth.login'); 
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Cart = new Cart ;
        $Cart->user_id = $request->UserId;
        $Cart->book_id = $request->BookId;
        $Cart->save();
        $UserId = auth::id();
        $Carts = Cart::where('user_id' , '=' , $UserId)
             ->join('books', 'books.id', '=', 'carts.book_id')
             ->join('users', 'users.id', '=', 'carts.user_id')
             ->get();   
        // return view('cart',['Carts'=>$Carts]);
       return redirect()->action('CartController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $UserId = Auth::id();
      Cart::where('book_id','=', $id)-> where('user_id','=', $UserId)->delete();
      return redirect()->action('CartController@index');

    }
}
