<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;
use App\Author;
use App\Cart ;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;  

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $Books = Book::orderBy('id') ->paginate(16);
        $Categories = Category::all();
        $Authors = Author::all();
        $UserId = auth::id();
        $Carts = Cart::where('user_id' , '=' , $UserId)
             ->join('books', 'books.id', '=', 'carts.book_id')
             ->get();
        return view('catalog' , [
            'Books' => $Books ,
            'Categories' => $Categories,
            'Authors' => $Authors,
            'Carts' => $Carts
    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
       
       $path = $request->file('Image')->store('public/BookImg');
       $book = new Book;
       $book->Publisher = $request ->Publisher;
       $book->Name = $request ->Name;
       $book->Price = $request ->Price;
       $book->Author = $request ->Author;
       $book->Description = $request ->Description;
       $book->Image = $path ;
       $book->Category = $request ->Category;
       $book->save();
       return redirect()->action('AdminController@booksCreate');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $UserId = auth::id();
        $Carts = Cart::where('user_id' , '=' , $UserId)
             ->join('books', 'books.id', '=', 'carts.book_id')
             ->get();
        $Book=Book::where('id','=',$id)->get();
        return view('single-product',[
            'Book' => $Book ,
            'Carts' => $Carts
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $path = $request->file('Image')->store('public/BookImg');
        $Books = Book::where('id', $id)
            ->update([
                        'Publisher' => $request->Publisher ,
                        'Name' => $request->Name,
                        'Price' => $request->Price ,
                        'Author' => $request->Author ,
                        'Description' => $request->Description ,
                        'Image' => $path ,
                        'Category' => $request->Category 
            ]);
       return redirect()->action('AdminController@booksview');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('books')->where('id', '=', $id)->delete();
    }

     public function search(Request $request)
    {
    
        $SearchQuery = $request->input('SearchQuery');
        $Books = Book::orderBy('id')-> where('Author' , 'like' ,'%' . $SearchQuery.'%' ) 
                                    ->orwhere('Name' , 'like' ,'%' . $SearchQuery.'%' )
                                    ->orwhere('Category' , 'like' , '%' . $SearchQuery.'%' )->paginate(16);
        $Categories = Category::all();
        $Authors = Author::all();
        $UserId = auth::id();
        $Carts = Cart::where('user_id' , '=' , $UserId)
             ->join('books', 'books.id', '=', 'carts.book_id')
             ->get();
                return view('catalog' , [
                    'Books' => $Books ,
                    'Categories' => $Categories,
                    'Carts' => $Carts,
                    'Authors' => $Authors
            ]);
                
            


    }
    

    public function CategoryFilter($Category)
    {
       

        
        $Books = Book::orderBy('id')-> where([       
                                                    ['Category', '=', $Category]
                                                ]) ->paginate(16);
        $Categories = Category::all();
        $Authors = Author::all();
        $UserId = auth::id();
        $Carts = Cart::where('user_id' , '=' , $UserId)
             ->join('books', 'books.id', '=', 'carts.book_id')
             ->get();
        return view('catalog' , [
            'Books' => $Books ,
            'Categories' => $Categories,
            'Authors' => $Authors,
            'Carts' => $Carts
    ]);
    }

    public function AuthorFilter($Author)
    {
       

        
        $Books = Book::orderBy('id')-> where([       
                                                ['Author', '=', $Author]
                                                ]) ->paginate(16);
        $Categories = Category::all();
        $Authors = Author::all();
        $UserId = auth::id();
        $Carts = Cart::where('user_id' , '=' , $UserId)
             ->join('books', 'books.id', '=', 'carts.book_id')
             ->get();
        return view('catalog' , [
            'Books' => $Books ,
            'Categories' => $Categories,
            'Carts' => $Carts,
            'Authors' => $Authors
    ]);
    }

}
