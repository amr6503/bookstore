<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// home page route
Route::get('/', 'MixedController@index' );

// login
Route::get('/home', 'HomeController@index')->name('home');

// about
Route::get('/about', function () { return view('about'); }  );


Route::get('/search','BookController@search');
// book request route
// Route::resource('/BookRequest', 'BookrequestController' );
// contact routes
Route::resource('/contactus','ContactController')->middleware('admin');
Route::resource('/books','BookController');
Route::resource('/author','AuthorController');
Route::resource('/category','CategoryController');
Route::resource('/Cart','CartController');
Route::resource('/Order','OrderController');



Route::group(['prefix'=>'books'] ,function (){

	Route::get('/Categories/{Category}','BookController@CategoryFilter');
	Route::get('/Authors/{Author}','BookController@AuthorFilter');
 });


Route::group(['prefix'=>'admin' , 'middleware'=>'admin']  ,function (){
	
	Route::get('/','AdminController@index');
	Route::get('/books','AdminController@booksview');
	Route::get('/booksAdd','AdminController@booksCreate');
	Route::get('/bookupdate/{id}','AdminController@booksupdate');
	Route::get('/categories','AdminController@categoriesview');
	Route::get('/categories/{id}','AdminController@categoriesupdate');
	Route::get('/authors','AdminController@authorsview');
	Route::get('/authors/{id}','AdminController@authorsupdate');
	Route::get('/orders','AdminController@ordersview');

 });
Auth::routes();



