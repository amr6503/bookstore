@extends('app')
@section('content')



<!-- Hero Slider -->
    <section class="hero">
   <img src="assets/img/1-min.jpg">
    </section> <!-- end hero slider -->

   
    <!-- Best Seller -->
    <section class="section-wrap pb-30">
      <div class="container">

        <div class="heading-row">
          <div class="text-center">
            <h2 class="heading bottom-line">
              الأكثر مبيعا
            </h2>
          </div>
        </div>

        <div class="row row-8">
          <!--start of the loop-->
          @foreach($Bestsellers as $row)
          <!--start of the product-->
          <div class="col-lg-2 col-sm-4 product">
            <div class="product__img-holder">
              <a href="/books/{{$row->id}}" class="product__link" aria-label="Product">
                <img src="{{ asset($row->Image)}}" alt="" class="product__img">
                <img src="{{ asset($row->Image)}}" alt="" class="product__img-back">
              </a>
              <div class="product__actions">
                <a href="quickview.html" class="product__quickview">
                  <i class="ui-eye"></i>
                  <span>Quick View</span>
                </a>
                <a href="#" class="product__add-to-wishlist">
                  <i class="ui-heart"></i>
                  <span>Wishlist</span>
                </a>
              </div>
            </div>

            <div class="product__details">
              <h3 class="product__title">
                <a href="single-product.html">{{ $row->Name }}</a>
              </h3>
            </div>

            <span class="product__price">
              <ins>
                <span class="amount"> جم .{{ $row->Price }}</span>
              </ins>
            </span>
          </div> <!-- end product -->
          @endforeach
          <!--end of the loop-->
        </div> <!-- end row -->
      </div> <!-- end container -->
    </section> <!-- end best seller -->

    <!-- New Arrivals -->
    <section class="section-wrap no-padding">
      <div class="container">

        <div class="heading-row">
          <div class="text-center">
            <h2 class="heading bottom-line">
              وصل حديثا
            </h2>
          </div>
        </div>

        <div class="row row-8">


         <!--start of the loop-->
          @foreach($Newarrivals as $row)
          <!--start of the product-->
          <div class="col-lg-2 col-sm-4 product">
            <div class="product__img-holder">
              <a href="/books/{{$row->id}}" class="product__link" aria-label="Product">
                <img src="{{ asset($row->Image)}}" alt="" class="product__img">
                <img src="{{ asset($row->Image)}}" alt="" class="product__img-back">
              </a>
              <div class="product__actions">
                <a href="quickview.html" class="product__quickview">
                  <i class="ui-eye"></i>
                  <span>Quick View</span>
                </a>
                <a href="#" class="product__add-to-wishlist">
                  <i class="ui-heart"></i>
                  <span>Wishlist</span>
                </a>
              </div>
            </div>

            <div class="product__details">
              <h3 class="product__title">
                <a href="single-product.html">{{ $row->Name }}</a>
              </h3>
            </div>

            <span class="product__price">
              <ins>
                <span class="amount"> جم .{{ $row->Price }}</span>
              </ins>
            </span>
          </div> <!-- end product -->
          @endforeach
          <!--end of the loop-->
    
        </div> <!-- end row -->
      </div> <!-- end container -->
    </section> <!-- end new arrivals -->

    



@endsection