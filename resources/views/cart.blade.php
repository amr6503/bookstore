@extends('app')
@section('content')
    <!-- Page Title -->
    <section class="page-title text-center">
      <div class="container">
        <h1 class=" heading page-title__title">سلة التسوق</h1>
      </div>
    </section> <!-- end page title -->


    <!-- Cart -->
    <section class="section-wrap cart pt-50 pb-40">
      <div class="container relative">

        <div class="table-wrap">
          <table class="shop_table cart table">
            <thead>
              <tr>
                <th class="product-name" colspan="2">اسم الكتاب </th>
                <th class="product-price" >السعر</th>
{{--                 <th class="product-quantity">Quantity</th>
                <th class="product-subtotal" colspan="2">Total</th> --}}
              </tr>
            </thead>
            <tbody>
              <?php $BooksArray = array(); ?> 
              @foreach($Carts as $Cart)
             
               
              <tr class="cart_item">
                <td class="product-thumbnail">
                  <a href="#">
                    <img src="{{ $Cart->Image }}" alt="{{ $Cart->Name }}">
                  </a>
                </td>
                <td class="product-name">
                  <a href="#">{{ $Cart->Name }}</a>
                </td>
                <td class="product-price">
                  <span class="amount">{{ $Cart->Price }}  جم</span>
                </td>
                <td class="product-price">
                  <span class="amount"></span>
                </td>
                {{-- <td class="product-quantity"> 
                  <div class="quantity buttons_added">
                    <input type="button" value="-" class="minus">
                    <input type="number" step="1" min="0" value="1" title="Qty" class="input-text qty text">
                    <input type="button" value="+" class="plus">
                  </div>
                </td>
                <td class="product-subtotal">
                  <span class="amount">$1250.00</span>
                </td> --}}
                <td class="product-remove">
                  <form  action="/Cart/{{ $Cart->id }}"  method="POST">
                    <button  type="submit" title="Remove this item" class="btn btn-danger" >حذف  <i class="ui-close"></i></a>
                    @CSRF @method('DELETE')
                  
                  </form>
                </td>
              </tr>
              @endforeach
                
            </tbody>
          </table>
        </div>

        <div class="row mb-30">
          <div class="col-lg-5">

          </div>

          <div class="col-lg-7">
            <div class="actions">
              
              <div class="wc-proceed-to-checkout">
                <form method="POST" action="/Order">
                  @CSRF
                  <button  type="submit" name="submit" class="btn btn-md btn-color btn-button">
                    <span>تأكيد الطلب </span>
                  </button>
                   <input type="hidden" name="BookId" value="{{ $Carts }}  ">
                   <input type="hidden" name="UserId" value="{{auth::id()}}">
                </form>
                
              </div>
            </div>
          </div>
        </div>

        <div class="row justify-content-between">
          <div class="col-lg-6 shipping-calculator-form">
            
            



                
          </div> <!-- end col shipping calculator -->

          <div class="col-lg-4">
            <div class="cart_totals">
              <h2 class="uppercase mb-20">اجمالي السلة  <span>{{$Carts->sum('Price')}} جم </span></h2>
{{-- 
              <table class="table shop_table">
                <tbody>
                  <tr class="cart-subtotal">
                    <th>Cart Subtotal</th>
                    <td>
                      <span class="amount">{{$Carts->sum('Price')}}</span>
                    </td>
                  </tr>
                  <tr class="shipping">
                    <th>Shipping</th>
                    <td>
                      <span>Free Shipping</span>
                    </td>
                  </tr>
                  <tr class="order-total">
                    <th>Order Total</th>
                    <td>
                      <strong><span class="amount">$1490.00</span></strong>
                    </td>
                  </tr>
                </tbody>
              </table> --}}

            </div>
          </div> <!-- end col cart totals -->

        </div> <!-- end row -->     

        
      </div> <!-- end container -->
    </section> <!-- end cart -->

@endsection