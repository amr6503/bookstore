@extends('app')
@section('content')

    <!-- Catalog -->
    <section class="section-wrap pt-60 pb-30 catalog">
      <div class="container">

        <!-- Breadcrumbs -->
        {{-- <ol class="breadcrumbs">
          <li>
            <a href="index.html">Home</a>
          </li>
          <li>
            <a href="index.html">Pages</a>
          </li>
          <li class="active">
            Catalog Grid
          </li>
        </ol> --}}

        <div class="row">
          <div class="col-lg-9 order-lg-2 mb-40">

            <!-- Filter -->          
            {{-- <div class="shop-filter">
              <p class="woocommerce-result-count">
                Showing: 1-12 of 80 results
              </p>
              <span class="woocommerce-ordering-label">Sort by</span>
              <form class="woocommerce-ordering">
                <select>
                  <option value="default-sorting">Default Sorting</option>
                  <option value="price-low-to-high">Price: high to low</option>
                  <option value="price-high-to-low">Price: low to high</option>
                  <option value="by-popularity">By Popularity</option>
                  <option value="date">By Newness</option>
                  <option value="rating">By Rating</option>
                </select>
              </form>
            </div> --}}

            <div class="row row-8">

              @foreach ($Books as $Book)
{{-- {{dd($Book->Image)}} --}}
            <div class=" product">
                  <div class="product__img-holder">
                    <a href="/books/{{$Book->id }}" class="product__link">
                      <img src='{{asset($Book->Image)}}' alt="" class="product__img ProductImageHeight ProductImageWidth">
                      <img src='{{asset($Book->Image)}}' alt="" class="product__img-back">
                    </a>
                    <div class="product__actions">
                      <a href="quickview.html" class="product__quickview">
                        <i class="ui-eye"></i>
                        <span>Quick View</span>
                      </a>
                      <a href="#" class="product__add-to-wishlist">
                        <i class="ui-heart"></i>
                        <span>Wishlist</span>
                      </a>
                    </div>
                  </div>

                  <div class="product__details">
                    <h3 class="product__title">
                      <a href="/books/{{ $Book->id }}" >{{ $Book->Name }}</a>
                    </h3>
                  </div>

                  <span class="product__price">
                    <ins>
                      <span class="amount">{{ $Book->Price }}  ﺟﻢ</span>
                    </ins>
                  </span>
                </div> <!-- end product -->

              @endforeach
           
            </div> <!-- end row -->
            
            <!-- Pagination -->
            <div class="pagination clearfix">                
              <nav class="pagination__nav right clearfix ">

              {{ $Books->links() }}
                <!--
                <span class="pagination__page pagination__page--current">1</span>
                <a href="#" class="pagination__page">2</a>
                <a href="#" class="pagination__page">3</a>
                <a href="#" class="pagination__page">4</a>
                -->
              </nav>
            </div>

          </div> <!-- end col -->


          <!-- Sidebar -->
          <aside class="col-lg-3 sidebar left-sidebar">

            <!-- Categories -->
             <div class="widget widget_categories widget--bottom-line">
              <h4 class="widget-title">اﻷﻗﺴﺎﻡ</h4>
              <ul>
                 @foreach($Categories as $Category)
                
                <li>
                  <a href="/books/Categories/{{ $Category->Category }}">{{ $Category->Category }}</a>
                </li>
                @endforeach
              
              </ul>
            </div>

            

            <!-- Color -->
            <div class="widget widget__filter-by-color widget--bottom-line">
              <h4 class="widget-title">الكاتب</h4>
              <ul class="color-select">
                 @foreach($Authors as $Author)
                  <li>
                    <a href="/books/Authors/{{ $Author->Name }}">{{ $Author->Name }}</a>
                  </li>
                 @endforeach
              
                
              </ul>
            </div>

            <!-- Filter by Price -->
            {{-- <div class="widget widget__filter-by-price widget--bottom-line">
              <h4 class="widget-title">Filter by Price</h4>
               
              <div id="slider-range"></div>
              <p>
                <label for="amount">Price:</label>
                <input type="text" id="amount">
                <a href="#" class="btn btn-sm btn-dark"><span>Filter</span></a>
              </p>
            </div> --}}

          </aside> <!-- end sidebar -->

        </div> <!-- end row -->
      </div> <!-- end container -->
    </section> <!-- end catalog -->

    @endsection

    