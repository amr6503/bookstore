@extends('admin.app')
@section('content')

<section class="m-3">
<a href="" class="btn btn-success">Create New Book</a>
</section>

<table class="table table-bordered table-hover">
  <thead class="thead-dark">
    <tr>
    					<th scope="col">id </th>
						<th scope="col">Publisher</th>
						<th scope="col">Name</th>
						<th scope="col">Price</th>
						<th scope="col">Author</th>
						<th scope="col">Description</th>
						<th scope="col">Category </th>
						<th scope="col">Image</th>
						<th scope="col">CRUD</th>
    </tr>
  </thead>
  <tbody>
						
			 @foreach ($Books as $Book)
			 <?php $id = $Book->id ?>
			 		<tr>
			 			<th scope="row">{{ $Book->id }}</th>
						<td>{{ $Book->Publisher }}</td>
						<td>{{ $Book->Name }}</td>
						<td>{{ $Book->Price }}</td>
						<td>{{ $Book->Author }}</td>
						<td>{{ $Book->Description }}</td>
						<td>{{ $Book->Category }}</td>
						<td><img src='{{asset($Book->Image)}}' alt="" class="product__img-back w-100"></td>
						<td class="d-flex justify-content-around ">
							<form method="POST" action="/books/{{$id}}">
								<button type="submit" class="btn btn-danger">Delete</button>
								<input type="hidden" name="_method" value="DELETE">
								@CSRF
							</form>
							<a href="/admin/bookupdate/{{$id}}" class="btn btn-primary">Update</a>
						</td>
					</tr>
              @endforeach
	</tbody>
</table>
         
          {{ $Books->links() }}

@endsection