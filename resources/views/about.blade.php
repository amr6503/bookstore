@extends('app')
@section('content')

    <!-- Page Title -->
    <section class="page-title text-center">
      <div class="container">
        <h1 class=" heading page-title__title">عن الشركة </h1>
        <p class="page-title__subtitle lead">مجموعة من الشباب تطمح الي نشر  الثقافة عن طريق تسهيل عملية شراء الكتب و البحث عنها <br> نعمل حاليا في محافظة قنا فقط و قريبا سنتوسع الي باقي محافظات مصر </p>
      </div>
    </section> <!-- end page title -->

    <!-- Our Team -->
    <section class="section-wrap pb-20">
      <div class="container">
        <div class="row row-8 d-flex justify-content-center">

          <div class="col-lg-3 col-sm-6 team text-center">
            <div class="team__img-holder">
              <img src="img/team/team_1.jpg" class="team__img" alt="">
            </div>
            <h4 class="team__title uppercase">عمرو ابراهيم </h4>
            <span class="team__position">مبرمج </span>
            <div class="socials socials--small mt-20">
              <a href="#" class="facebook"><i class="ui-facebook"></i></a>
              <a href="#" class="twitter"><i class="ui-twitter"></i></a>
              <a href="#" class="snapchat"><i class="ui-snapchat"></i></a>
              <a href="#" class="instagram"><i class="ui-instagram"></i></a>
              <a href="#" class="pinterest"><i class="ui-pinterest"></i></a>
            </div>        
          </div> <!-- end team member -->

          <div class="col-lg-3 col-sm-6 team text-center">
            <div class="team__img-holder">
              <img src="img/team/team_2.jpg" class="team__img" alt="">
            </div>
            <h4 class="team__title uppercase">اسراء وردان </h4>
            <span class="team__position">اخصائية تسويق الكتروني</span>
            <div class="socials socials--small mt-20">
              <a href="#" class="facebook"><i class="ui-facebook"></i></a>
              <a href="#" class="twitter"><i class="ui-twitter"></i></a>
              <a href="#" class="snapchat"><i class="ui-snapchat"></i></a>
              <a href="#" class="instagram"><i class="ui-instagram"></i></a>
              <a href="#" class="pinterest"><i class="ui-pinterest"></i></a>
            </div>         
          </div> <!-- end team member -->


        </div>
      </div>
    </section> <!-- end our team -->

    <div class="container">
      <hr class="no-margin">
    </div>

    <!-- Testimonials -->
  {{--   <section class="section-wrap">
      <div class="container">

        <div class="heading-row mb-0">
          <div class="text-center">
            <h2 class="heading">
              What the customers say?
            </h2>
          </div>
        </div>

        <div class="row justify-content-center">
          <div class="col-md-8">

            <div id="owl-testimonials" class="owl-carousel owl-theme owl-carousel--dark-arrows owl-carousel--visible-arrows">

              <div class="testimonial">
                <div class="testimonial__rating text-center">
                  <span class="rating"></span>
                  <span class="rating__time">20 days ago</span>
                </div>
                <p class="testimonial__text">I’am amazed, I should say thank you so much for your awesome template. Design is so good and neat, every detail has been taken care these team are realy amazing and talented!</p>
                <span class="testimonial__author">Camille Ragpa</span>
              </div>

              <div class="testimonial">
                <div class="testimonial__rating text-center">
                  <span class="rating"></span>
                  <span class="rating__time">20 days ago</span>
                </div>
                <p class="testimonial__text">I’am amazed, I should say thank you so much for your awesome template. Design is so good and neat, every detail has been taken care these team are realy amazing and talented!</p>
                <span class="testimonial__author">Camille Ragpa</span>
              </div>

              <div class="testimonial">
                <div class="testimonial__rating text-center">
                  <span class="rating"></span>
                  <span class="rating__time">20 days ago</span>
                </div>
                <p class="testimonial__text">I’am amazed, I should say thank you so much for your awesome template. Design is so good and neat, every detail has been taken care these team are realy amazing and talented!</p>
                <span class="testimonial__author">Camille Ragpa</span>
              </div>

            </div> <!-- end carousel -->
          </div>
        </div> <!-- end carousel row -->

      </div>
    </section> <!-- end testimonials --> --}}
    @endsection
  
   