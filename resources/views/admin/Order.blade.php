@extends('admin.app')
@section('content')

<section class="m-3">
<a href="" class="btn btn-success">Create New Book</a>
</section>

<table class="table table-bordered table-hover">
  <thead class="thead-dark">
    <tr>
    					<th scope="col">id </th>
						<th scope="col">Publisher</th>
						<th scope="col">Name</th>
						<th scope="col">Price</th>
						<th scope="col">Author</th>
						<th scope="col">Description</th>
						<th scope="col">Category </th>
						<th scope="col">Image</th>
						<th scope="col">CRUD</th>
    </tr>
  </thead>
  <tbody>		
			 @foreach($Orders as $Order )
			 		<tr>
						<td>{{ $Order->Name }}</td>
						<td>{{ $Order->Price }}</td>
						<td><img src='' alt="" class="product__img-back"></td>
						<td class="d-flex justify-content-around ">
							<form method="POST" action="/books/">
								<button type="submit" class="btn btn-danger">Delete</button>
								<input type="hidden" name="_method" value="DELETE">
								@CSRF
							</form>
							<a href="/bookupdate/" class="btn btn-primary">Update</a>
						</td>
					</tr>
              @endforeach
	</tbody>
</table>
         


@endsection