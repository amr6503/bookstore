@extends('admin.app')
@section('content')

<section class="m-3">
	<form method="POST" action="/category">
		<input type="text" name="Category">
		<input class="btn btn-success" type="submit" name="submit">
		@CSRF
	</form>
</section>

<table class="table table-bordered table-hover">
  <thead class="thead-dark">
    <tr>
    					<th scope="col">id </th>
						<th scope="col">Category </th>
						<th scope="col">CRUD </th>

    </tr>
  </thead>
  <tbody>
						
			 @foreach ($Categories as $Category)
			 <?php $id = $Category->Id ?>
			 		<tr>
			 			<th scope="row">{{ $Category->Id }}</th>
						<td>{{ $Category->Category }}</td>
						<td class="d-flex justify-content-around ">
							<form method="POST" action="/category/{{$id}}">
								<button type="submit" class="btn btn-danger">Delete</button>
								@method("DELETE")
								@CSRF
							</form>
							<a href="/admin/categories/{{$id}}" class="btn btn-primary">Update</a>
						</td>
					</tr>
              @endforeach
	</tbody>
</table>
         
          

@endsection