@extends('admin.app')
@section('content')


<?php $id = $Books[0]->id ;?>
<table class="table table-bordered table-hover">
	<form method="POST" action="/books/{{$id}}" enctype="multipart/form-data">
      					<tr scope="col">
	    					<td>id</td>
				 			<td>{{ $Books[0]->id }}</td>
				 			<td></td>
    					</tr>
						<tr scope="col">
							<td>Publisher</td>
				 			<td><input type="text" name="Publisher" value="{{ $Books[0]->Publisher }}"> </td>
				 			<td></td>
						</tr>
						<tr scope="col">
							<td>Name</td>
				 			<td><input type="text" name="Name" value="{{ $Books[0]->Name }}"> </td>
				 			<td></td>
						</tr>
						<tr scope="col">
							<td>Price</td>
				 			<td><input type="text" name="Price" value="{{ $Books[0]->Price }}"> </td>
				 			<td></td>
						</tr>
						<tr scope="col">
							<td>Author</td>
				 			<td><input type="text" name="Author" value="{{ $Books[0]->Author }}"> </td>
				 			<td></td>
						</tr>
						<tr scope="col">
							<td>Description</td>
				 			<td><textarea name="Description" value="{{ $Books[0]->Description }}"> {{ $Books[0]->Description }} </textarea></td>
				 			<td></td>
						</tr>
						<tr scope="col">
							<td>Category</td>
				 			<td><input type="text" name="Category" value="{{ $Books[0]->Category }}"> </td>
						</tr>
						<tr scope="col">
							<td>Image</td>
				 			<td><img src=" {{asset( $Books[0]->Image) }}"></td>
							<td><input type="file"  name="Image" value="{{ $Books[0]->Image }}"></td>	
							
						</tr>
						<tr scope="col">
							<td>Update</td>
							<td><button type="submit" class="btn btn-primary">Update</button></td>
							<td></td>
							@method('PUT')
								@CSRF
						</tr>
    
		</form>
 

</table>
         
          

@endsection