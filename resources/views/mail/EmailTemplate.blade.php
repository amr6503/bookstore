@component('mail::message')
# Introduction

the name is : {{ $contact->name }}  <br><br>
the email is : {{ $contact->email }}  <br><br>
the subject is : {{ $contact->subject }}  <br><br>
the message is : {{ $contact->message }}  <br><br>


@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
