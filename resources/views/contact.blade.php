@extends('app')
@section('content')
  <title>تواصل معنا </title>

  



    <!-- Contact -->
    <section class="section-wrap pb-40">
      <div class="container">
        <div class="row">

          <div class="col-lg-8">
            <h2 class="uppercase">اترك رسالتك هنا </h2>
            <p></p>

            <!-- Contact Form -->
            <form id="contact-form" class="contact-form mt-30 mb-30" method="post" action="/contactus">
              <div class="contact-name">
                <label for="name">الاسم <abbr title="required" class="required">*</abbr></label>
                <input name="name" id="name" type="text">
              </div>
              <div class="contact-email">
                <label for="email">البريد الالكتروني  <abbr title="required" class="required">*</abbr></label>
                <input name="email" id="email" type="email">
              </div>
              <div class="contact-subject">
                <label for="email">الموضوع</label>
                <input name="subject" id="subject" type="text">
              </div>
              <div class="contact-message">
                <label for="message">الرسالة  <abbr title="required" class="required">*</abbr></label>
                <textarea id="message" name="message" rows="7" required="required"></textarea>
              </div>

              <input type="submit" class="btn btn-lg btn-color btn-button" value="Send Message" >
              <div id="msg" class="message"></div>
              @CSRF
            </form>
          </div> <!-- end col -->

          <div class="col-lg-4">
            <div class="contact-info">
              <ul>
                <li class="contact-info__item">
                  <h4 class="contact-info__title uppercase">أماكت تواجدنا </h4>
                  <h6 class="contact-info__store-title">محافظة قنا  - مدينة  قنا</h6>
                  <address class="address"></address>
                    
                </li>
                <li class="contact-info__item">
                  <h4 class="contact-info__title uppercase">معلومات  التواصل</h4>
                  <ul>
                    <li><span>موبايل: </span><a href="tel:01061016045">01061016045</a></li>
                    <li><span>بريد الكتروني : </span><a href="info@souqalkotob.space">info@souqalkotob.space</a></li>
                  </ul>
                </li>
                <li class="contact-info__item">
                  <h4 class="contact-info__title uppercase">ساعات العمل </h4>
                  <ul>
                    <li>اطوال ايام االأسبوع : 10صباحا  الي  10 مساءا</li>
                  </ul>
                </li>
              </ul>
              
            </div>
          </div>

        </div>
      </div>
    </section> <!-- end contact -->


  

@endsection