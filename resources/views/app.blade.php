<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155541545-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-155541545-1');
</script>

    
  <title> سوق الكتب </title>

  
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="">

  <!-- Google Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Questrial:400%7CMontserrat:300,400,700,700i' rel='stylesheet'>

  <!-- Css -->
  <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/css/font-icons.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/css/color.css')}}" />

  <!-- Favicons -->
  <link rel="shortcut icon" href="{{asset('assets/img/logo2.png')}}">
  <link rel="apple-touch-icon" href="{{asset('assets/img/apple-touch-icon.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/img/apple-touch-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/img/apple-touch-icon-114x114.png')}}">

</head>

<body>

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      <div></div>
    </div>
  </div>
  

  <!-- Mobile Sidenav -->    
  <header class="sidenav" id="sidenav">
    <!-- Search -->
    <div class="sidenav__search-mobile">
      <form method="get" class="sidenav__search-mobile-form" action="/search">
        <input type="search" class="sidenav__search-mobile-input" placeholder="Search..." aria-label="Search input" name="SearchQuery">
        <button type="submit" class="sidenav__search-mobile-submit" aria-label="Submit search">
          <i class="ui-search"></i>
        </button>
      </form>
    </div>

    <nav>
      <ul class="sidenav__menu" role="menubar">
        <li>
          <a href="/" class="sidenav__menu-link"> الرئيسية </a>
        </li>
      
        <li>
          <a href="/books" class="sidenav__menu-link">تصفح الكتب</a>
        </li>
        
        <li>
          <a href="/about" class="sidenav__menu-link">عن الشركة </a>
        </li>
        
        <li>
          <a href="/BookRequest" class="sidenav__menu-link">اطلب كتاب </a>
        </li>

        <li>
          <a href="/contactus" class="sidenav__menu-link"> تواصل معنا</a>
        </li>

        <li>
          <a href="/login" class="sidenav__menu-link">تسجيل الدخول </a>
        </li>
      </ul>
    </nav>
  </header> <!-- end mobile sidenav -->


  <main class="main oh" id="main">

    <!-- Navigation -->
    <header class="nav">

      <div class="nav__holder nav--sticky">
        <div class="container relative">

          <!-- Top Bar -->
          <div class="top-bar d-none d-lg-flex">

            <!-- Currency / Language -->
            <ul class="top-bar__currency-language">
                         
            </ul>

            <!-- Promo -->
            <p class="top-bar__promo text-center">تبحث عن كتاب و لم تتمكن من العثور عليه , 
                <a href = "/BookRequest"> اطلب الكتاب منا</a> 
            </p>
            <!-- Sign in / Wishlist / Cart -->
            <div class="top-bar__right">

              <!-- Sign In -->
              @if(Auth::check()) {{auth::user()->name}} @else
              <a href="/login" ><i class="ui-user"></i>تسجيل الدخول </a>
             @endif
              <!-- Wishlist -->
              <a href="#" class="top-bar__item"><i class="ui-heart"></i></a>

              <div class="top-bar__item nav-cart">  
              @if(Auth::check() && isset($Carts))                
                <a href="/Cart">
                  <i class="ui-bag"></i>( @if (count($Carts) > 0) {{count($Carts)}} @endif  )
                </a>
                
                <div class="nav-cart__dropdown">
                  <div class="nav-cart__items">

                    {{-- loop to get the cart item in the navbar --}}
                    @foreach($Carts as $Cart)
                    <div class="nav-cart__item clearfix">
                      <div class="nav-cart__title">
                        <a href="/books/{{$Cart->book_id}}">
                         {{$Cart->Name}}
                        </a>
                        <div class="nav-cart__price">
                          {{-- <span>1 x</span> --}}
                          <span>{{$Cart->Price}}</span>
                        </div>
                      </div>  
                      
                    </div>
                    @endforeach

                    

                  </div> <!-- end cart items -->

                  <div class="nav-cart__summary">
                    <span>اجمالي المبلغ</span>
                    <span class="nav-cart__total-price">{{$Carts->sum('Price')}}</span>
                  </div>

                  <div class="nav-cart__actions mt-20">
                    <a href="/Cart" class="btn btn-md btn-light"><span>تصفح السلة </span></a>
                    
                  </div>
                </div>
                 @endif 
              </div>
            </div>

          </div> <!-- end top bar -->

          <div class="flex-parent">

            <!-- Mobile Menu Button -->
            <button class="nav-icon-toggle" id="nav-icon-toggle" aria-label="Open mobile menu">
              <span class="nav-icon-toggle__box">
                <span class="nav-icon-toggle__inner"></span>
              </span>
            </button> <!-- end mobile menu button -->

            <!-- Logo -->
            <a href="/" class="logo">
              <img class="logo__img" src="{{asset('assets/img/logo2.png') }}" alt="logo">
            </a>

            <!-- Nav-wrap -->
            <nav class="flex-child nav__wrap d-none d-lg-block">              
              <ul class="nav__menu">

                <li class="nav__dropdown active">
                  <a href="/">الرئيسية</a>
                </li>

                <li class="nav__dropdown">
                  <a href="/books"> تصفح الكتب </a>
                </li>

                <li class="nav__dropdown">
                  <a href="/about">عن الشركة </a>
                </li>

                <li class="nav__dropdown">
                  <a href="/BookRequest"> اطلب كتاب </a>
                </li>
                
                <li class="nav__dropdown">
                  <a href="/contactus">تواصل معنا </a>
                </li>

              </ul> <!-- end menu -->

            </nav> <!-- end nav-wrap -->


            <!-- Search -->
            <div class="flex-child nav__search d-none d-lg-block">
              <form method="get" class="nav__search-form" action="/search">
                
                <input type="search" class="nav__search-input" placeholder=" ابحث عن كتاب " name="SearchQuery">
                <button type="submit" class="nav__search-submit">
                  {{-- <i class="ui-search"></i> --}}
                </button>
              </form>
            </div>


            <!-- Mobile Wishlist -->
            {{-- <a href="#" class="nav__mobile-wishlist d-lg-none" aria-label="Mobile wishlist">
              <i class="ui-heart"></i>
            </a> --}}

            <!-- Mobile Cart -->
            <a href="cart.html" class="nav__mobile-cart d-lg-none">
              <i class="ui-bag"></i>
              <span class="nav__mobile-cart-amount">(2)</span>
            </a>

            
         
        
          </div> <!-- end flex-parent -->
        </div> <!-- end container -->

      </div>
    </header> <!-- end navigation -->
     @yield('content')
    

    <!-- Footer -->
    <footer class="footer w-100">
      <div class="container  text-center p-3">
        <i  class=" "> حقوق النشر محفوظة </i>
      </div> 
      <!-- end bottom footer -->
    </footer> 
    <!-- end footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->


  
  <!-- jQuery Scripts -->
  <script type="text/javascript" src="{{asset('assets/js/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/easing.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/owl-carousel.min.js')}}"></script>  
  <script type="text/javascript" src="{{asset('assets/js/flickity.pkgd.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/modernizr.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/scripts.js')}}"></script>

</body>
</html>